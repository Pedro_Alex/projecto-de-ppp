#include "estruturas.h"

listaAlunos dataAlunos(void) {
    listaAlunos aux;
    aux = (listaAlunos) malloc (sizeof(noListaAlunos));
    if (aux != NULL){
        aux->student.numAluno=0;
        aux->student.anoAluno=0;
        aux->student.curso[0] = '\0';
        aux->student.regime = 0;
        aux->next = NULL;
    }
    return aux;
}

int listaAlunoVazia(listaAlunos baseAluno) {
return (baseAluno->next == NULL ? 1 : 0);
}

listaAlunos destroiListaAlunos(listaAlunos baseAluno) {
    listaAlunos tem_ptr;
    while(listaAlunoVazia(baseAluno)==0) {
        tem_ptr = baseAluno;
        baseAluno = baseAluno->next;
        free(tem_ptr);
    }
    free(baseAluno);
    return NULL;
}

int eliminaNoAluno (listaAlunos baseAluno, int num) {
    listaAlunos ant = baseAluno->next;
    listaAlunos actual = ant->next;
    if(ant->student.numAluno == num){
        if(ant->next == NULL){
            baseAluno->next = NULL;
            free(ant);
            return 1;
        }
        else{
            baseAluno->next = ant->next;
            free(ant);
            return 1;
        }
    }
    else{
        while(actual != NULL){
            if(actual->student.numAluno == num){
                break;
            }
            else{
                ant = ant->next;
                actual = actual->next;
            }
        }
        if (actual != NULL) {
            ant->next = actual->next;
            free (actual);
            return 1;
        }
        else if(actual == NULL){
            ant->next = NULL;
            free (actual);
            return 1;
        }
    }
    return 0;
}

void printAlunoLista(listaAlunos teste) {
    listaAlunos tmp = teste->next;
    printf("Lista de Alunos:\n\n");
    while(tmp != NULL){
        printf("Numero: %d\n", tmp->student.numAluno);
        printf("Ano: %d\n", tmp->student.anoAluno);
        if (tmp->student.regime == 1) printf("Regime Normal.\n");
        else if (tmp->student.regime == 2) printf("Regime: Trabalhador-Estudante.\n");
        else if (tmp->student.regime == 3) printf("Regime: Atleta.\n");
        else if (tmp->student.regime == 4) printf("Regime: Dirigente Associativo.\n");
        else if (tmp->student.regime == 5) printf("Regime: Erasmus.\n");
        printf("Curso: %s\n\n", tmp->student.curso);
        tmp = tmp->next;
    }
    fflush(stdin);
    getchar();
}

listaAlunos procura_listaAlunos(listaAlunos baseAluno, int num) {
    listaAlunos ant = baseAluno->next;
    listaAlunos atual = ant->next;
    while(ant != NULL){
        if ( ant->student.numAluno == num) {
            return ant;
        }
        ant = ant->next;
        atual = atual->next;
    }
    return 0;
}

void criaAluno(listaAlunos baseAluno) {
    int flag;
    listaAlunos novoAluno = (listaAlunos) malloc(sizeof(noListaAlunos));
    novoAluno->next = NULL;

    while(flag!=0) {
        flag=0;
        printf("Numero do aluno: ");
        fflush(stdin);
        if((scanf("%d",&novoAluno->student.numAluno)!=1) || (verificaNumAluno(novoAluno->student.numAluno)!=1)) {
            printf("\n\tNumero de estudante Invalido.\n");
            flag=1;
        }
    }
    printf("Ano do aluno: ");
    fflush(stdin);
    scanf("%d",&novoAluno->student.anoAluno);

    printf("\n1 - Regime Normal.\n");
    printf("2 - Regime Trabalhador-Estudante.\n");
    printf("3 - Regime Atleta.\n");
    printf("4 - Regime Dirigente Associativo.\n");
    printf("5 - Regime Erasmus.\n");
    fflush(stdin);
    scanf("%d",&novoAluno->student.regime);

    printf("Nome do curso: ");
    fflush(stdin);
    gets(novoAluno->student.curso);
    strupr(novoAluno->student.curso);
    if(baseAluno -> next == NULL){
        baseAluno->next = novoAluno;
    }
    else{
        listaAlunos aux = baseAluno;
        while(aux -> next != NULL){    /* se eu meter aux, deixa de funcionar não sei porque (???) */
            aux = aux->next;
        }
        aux -> next = novoAluno;
    }
    printf("\n\tALUNO CRIADO!\t");
    getchar();
}

void alteraAluno(listaAlunos baseAluno) {
    int flag,num,opcao2=0, novoNum, novoAno, novoRegime;
    listaAlunos tmp = baseAluno->next;
    printf("Introduza o numero do aluno que deseja alterar: ");
    while(flag!=0) {
        flag=0;
        fflush(stdin);
        if(scanf("%d",&num)!=1 || procura_listaAlunos(baseAluno,num)==0){
            printf("Numero de estudante invalido.\n");
            flag=1;
        }
    }
    if(tmp->student.numAluno == num) {
    while(!opcao2) {
        printf("\nO que pretende alterar?\n");
        printf("1 - O numero do aluno.\n");
        printf("2 - O ano do aluno.\n");
        printf("3 - O regime do aluno.\n");
        printf("4 - O curso do aluno.\n");
        printf("Opçao: ");
        fflush(stdin);
        scanf("%d", &opcao2);
        switch(opcao2){
            case 1:{
                printf("\nO novo numero é: ");
                if(scanf("%d", &novoNum) == 1 && verificaNumAluno(novoNum) == 1){
                    tmp->student.numAluno = novoNum;
                    break;
                }
            }
            case 2:{
                printf("\nO novo ano é: ");
                fflush(stdin);
                scanf("%d", &novoAno);
                tmp->student.anoAluno = novoAno;
                break;
            }
            case 3: {
                printf("\nO novo regime é: ");
                fflush(stdin);
                scanf("%d", &novoRegime);
                tmp->student.regime = novoRegime;
                break;
            }
            case 4: {
                printf("\nO novo curso é: ");
                fflush(stdin);
                gets(tmp->student.curso);
                break;
            }
            default: {
                printf("\n\tOpcao invalida, prima enter para voltar ao menu\n");
                fflush(stdin);
                getchar();
            }
        }
        fflush(stdin);
        getchar();
        }
    }
    else{
        while(tmp != NULL) {
            if(tmp->student.numAluno == num) {
                while(!opcao2) {
                    printf("\nO que pretende alterar?\n");
                    printf("1 - O numero do aluno.\n");
                    printf("2 - O ano do aluno.\n");
                    printf("3 - O regime do aluno.\n");
                    printf("4 - O curso do aluno.\n");
                    printf("Opçao: ");
                    fflush(stdin);
                    scanf("%d", &opcao2);
                    switch(opcao2){
                        case 1:{
                            printf("\nO novo numero é: ");
                            if(scanf("%d", &novoNum) == 1 && verificaNumAluno(novoNum) == 1){
                                tmp->student.numAluno = novoNum;
                                break;
                            }
                        }
                        case 2:{
                            printf("\nO novo ano é: ");
                            fflush(stdin);
                            scanf("%d", &novoAno);
                            tmp->student.anoAluno = novoAno;
                            break;
                        }
                        case 3: {
                            printf("\nO novo regime é: ");
                            fflush(stdin);
                            scanf("%d", &novoRegime);
                            tmp->student.regime = novoRegime;
                            break;
                        }
                        case 4: {
                            printf("\nO novo curso é: ");
                            fflush(stdin);
                            gets(tmp->student.curso);
                            break;
                        }
                        default: {
                            printf("\n\tOpcao invalida, prima enter para voltar ao menu\n");
                            fflush(stdin);
                            getchar();
                        }
                        printf("\n\tALUNO ALTERADO COM SUCESSO.\n");
                        fflush(stdin);
                        getchar();
                    }
                }
            }
            else tmp = tmp->next;
        }
    }

}

void eliminaAluno(listaAlunos baseAluno) {
    int num,flag;
    printf("Numero do Aluno: ");
    while(flag!=0) {
            flag=0;
        if(scanf("%d",&num)!=1  || verificaNumAluno(num)!=1 || procura_listaAlunos(baseAluno,num)==0) {
            printf("Numero de estudante invalido.\n");
            flag==1;
            flag=1;
        }
    }
    if (eliminaNoAluno(baseAluno,num)==1) printf("Aluno removido com sucesso.\n");
    else printf("Nao foi possivel remover o aluno %d",num);
}

/*O aluno ja esta todo a funcionar*/

listaDisciplinas dataDisciplinas(void) {
    listaDisciplinas aux = (listaDisciplinas) malloc(sizeof(noListaDisciplinas));
    if(aux!=NULL) {
        aux->d.nomeDisciplina[0] = '\0';
        aux->d.nomeDocente[0] = '\0';
        aux->next = NULL;
    }
    return aux;
}

void printDisciplinaLista(listaDisciplinas teste){
    listaDisciplinas tmp = teste->next;
    printf("Lista de Disciplinas:\n\n");
    while(tmp != NULL){
        printf("Disciplina: %s\n", tmp->d.nomeDisciplina);
        printf("Docente: %s\n\n", tmp->d.nomeDocente);
        tmp = tmp->next;
    }
    printf("\n");
    fflush(stdin);
    getchar();
}

listaDisciplinas procura_listaDisciplinas(listaDisciplinas baseDisciplina, char nome[]) {
    listaDisciplinas ant = baseDisciplina;
    listaDisciplinas atual = ant->next;
    while(atual != NULL){
        if ( strcmp(ant->d.nomeDisciplina,nome)==0) return ant;/*devolve 1 se nao encontrar o nó*/
        ant = atual;
        atual = atual->next;
    }
    return NULL;
}

int eliminaNoDisciplina (listaDisciplinas baseDisciplina, char nome[]) {
    listaDisciplinas ant = baseDisciplina->next;
    if (ant == NULL) return 0;
    else {
        listaDisciplinas actual = ant->next;
        if(strcmp(ant->d.nomeDisciplina,nome)==1){
            if(ant->next == NULL){
                baseDisciplina->next = NULL;
                free(ant);
            }
            else{
                baseDisciplina->next = ant->next;
                free(ant);
            }
            return 1;
        }
        else{
            while(actual != NULL){
                if(strcmp(actual->d.nomeDisciplina,nome)==1) break;
                else{
                    ant = ant->next;
                    actual = actual->next;
                }
            }
            if (actual != NULL) {
                ant->next = actual->next;
                free (actual);
            }
            else if(actual == NULL){
                ant->next = NULL;
                free (actual);
            }
        }
    }
}

void criaDisciplina(listaDisciplinas baseDisciplina) {
    listaDisciplinas tmp = baseDisciplina;
    listaDisciplinas novaDisciplina = (listaDisciplinas) malloc (sizeof(noListaDisciplinas));
    novaDisciplina->next = NULL;
    printf("Nome da Disciplina: ");
    fflush(stdin);
    gets(novaDisciplina->d.nomeDisciplina);
    strupr(novaDisciplina->d.nomeDisciplina);
    printf("Regente da Disciplina: ");
    fflush(stdin);
    gets(novaDisciplina->d.nomeDocente);
    while(tmp->next != NULL) tmp = tmp->next;
    tmp->next = novaDisciplina;
    printf("\n\t DISCIPLINA CRIADA!");
    fflush(stdin);
    getchar();
}

void alteraDisciplina(listaDisciplinas baseDisciplina) {
    listaDisciplinas tmp = baseDisciplina->next;
    int flag;
    char var[MAX];
    while(flag!=0) {
        flag=0;
        fflush(stdin);
        gets(var);
        if(procura_listaDisciplinas(tmp,var)==0) {
            printf("Disciplina invalida.\n");
            flag=1;
        }
    }
    printf("Entrou!!");
}

void eliminaDisciplina(listaDisciplinas baseDisciplina) {
    int flag;
    char nome[MAX];
    while(flag!=0) {
            flag=0;
            gets(nome);
        if(procura_listaDisciplinas(baseDisciplina, nome)==0) {
            printf("A disciplina %s nao existe.\n", nome);
            flag==1;
        }
    }
    eliminaNoDisciplina(baseDisciplina,nome);
}



listaExames dataExames(void) {
    listaExames aux = (listaExames) malloc(sizeof(noListaExames));
    if(aux!=NULL) {
        aux->e.d.nomeDisciplina[0] = '\0';
        aux->e.d.nomeDocente[0] = '\0';
        aux->e.dataExame = (listaExames) malloc(sizeof(Data));
        aux->e.epocaExame = NULL;
        aux->e.sala = NULL;
        aux->next = NULL;
    }
    return aux;
}

int procura_listaExames(listaExames baseExame, char exame[]) {
    listaExames ant = baseExame;
    listaExames atual = baseExame->next;
    while(atual != NULL)
    {
        if (strcmp(ant->e.d.nomeDisciplina,exame)) {
            return ant;
        }/*devolve 1 se encontrar o nó*/
        ant = atual;
        atual = atual->next;
    }
    return 0;
}

int listaExamesVazia(listaExames baseExame) {
    return (baseExame->next == NULL ? 1 : 0);
}

listaExames destroiListaExames(listaExames baseExame) {
    listaExames tem_ptr;
    while(listaExamesVazia(baseExame)==0) {
        tem_ptr = baseExame;
        baseExame = baseExame->next;
        free(tem_ptr);
    }
    free(baseExame);
    return NULL;
}

void eliminaNoExame (listaExames baseExame, char nome[]) {
    listaExames ant;
    listaExames actual;
    procura_listaExames(baseExame, nome);
    if (actual != NULL) {
        ant->next = actual->next;
        free (actual);
    }
}

void criaExame(listaExames baseExame) {
    int flag1, flag2;
    char nome[MAX];
    listaExames tmp = baseExame;
    listaExames novoExame = dataExames();

    printf("Qual e a disciplina do exame?");
    while(flag1!=0) {
        flag1=0;
        fflush(stdin);
        gets(nome);
        strupr(nome);
        if(procura_listaDisciplinas(tmp,nome)==0) {
            printf("Disciplina nao existe.\n");
            flag1=1;
        }
    }
    strcpy(nome,novoExame->e.d.nomeDisciplina);

    while(flag1!=0 || flag2!=0) {
        flag1=0;
        flag2=0;
        printf("Data do Exame(dd mm aaaa):\n");
        fflush(stdin);
        scanf("%d %d %d",&novoExame->e.dataExame->dia, &novoExame->e.dataExame->mes, &novoExame->e.dataExame->ano);
        if(verificaData(novoExame->e.dataExame->dia, &novoExame->e.dataExame->mes, &novoExame->e.dataExame->ano)!=1) {
            flag2=1;
            printf("A data nao esta correta.\n");
        }
        printf("Hora do Exame(hh mm):\n");
        fflush(stdin);
        scanf("%d %d", &novoExame->e.dataExame->hora, &novoExame->e.dataExame->minutos);

        novoExame->e.duracao = horasParaMinutos(novoExame->e.dataExame->hora,novoExame->e.dataExame->minutos);
        if(verificaDataExame(tmp,novoExame->e.dataExame)!=1) {
            printf("Horario Indisponivel, escolha outro.\n");
            flag1 =1;
        }
    }

    /*printf("Data: %d/%d/%d\n Hora: %d:%d\n",novoExame->e.dataExame.dia, novoExame->e.dataExame.mes, novoExame->e.dataExame.ano, novoExame->e.dataExame.hora, novoExame->e.dataExame.minutos);*/

    printf("Duracao do exame (minutos): ");
    fflush(stdin);
    scanf("%d", &novoExame->e.duracao);

    printf("Epoca do Exame:\n");
    printf("1 - Normal.\n");
    printf("2 - Recurso.\n");
    printf("3 - Especial.\n");
    fflush(stdin);
    scanf("%d", &novoExame->e.epocaExame);



    printf("Sala do Exame:\n");
    fflush(stdin);
    scanf("%d", &novoExame->e.sala);

    while(tmp->next != NULL) tmp = tmp->next;
    tmp->next = novoExame;

}
