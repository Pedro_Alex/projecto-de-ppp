#include "estruturas.h"

void menuAluno(listaAlunos baseAluno) {
    int opcao=0;
    while(!opcao) {
        printf("1 - Adicionar aluno.\n");
        printf("2 - Alterar informações do aluno.\n");
        printf("3 - Remover Aluno.\n");
        printf("4 - Listar Alunos.\n");
        printf("0 - Retroceder.\n");

        fflush(stdin);
        scanf("%d", &opcao);
        switch(opcao) {
            system("cls");
            case 1: {
                criaAluno(baseAluno);
                system("cls");
                opcao=0;
                break;
            }
            case 2: {
                alteraAluno(baseAluno);
                system("cls");
                opcao=0;
                break;
            }
            case 3: {
                eliminaAluno(baseAluno);
                system("cls");
                opcao=0;
                break;
            }
            case 4:
                printAlunoLista(baseAluno);
                system("cls");
                opcao = 0;
                break;
            case 0: {
                return;
            }
            default:{
                printf("\n\tOpcao invalida, prima enter para voltar ao menu");
                fflush(stdin);
                getchar();
                system("cls");
                opcao = 0;
            }
        }
    }
}

void menuDisciplina(listaDisciplinas baseDisciplina) {
    int opcao=0;
    while(!opcao) {
        printf("1 - Criar Disciplina.\n");
        printf("2 - Alterar Regente da Disciplina.\n");
        printf("3 - Remover Disciplina.\n");
        printf("4 - Listar Disciplinas.\n");
        printf("0 - Retroceder.\n");
        fflush(stdin);
        scanf("%d",&opcao);
        system("cls");

        switch(opcao) {
            system("cls");
            case 1:
                criaDisciplina(baseDisciplina);
                system("cls");
                opcao=0;
                break;
            case 2:
                alteraDisciplina(baseDisciplina);
                system("cls");
                opcao = 0;
                break;
            case 3:
                eliminaDisciplina(baseDisciplina);
                system("cls");
                opcao=0;
                break;
            case 4:
                printDisciplinaLista(baseDisciplina);
                system("cls");
                opcao = 0;
                break;
            case 0:
                return;
            default: {
                printf("\n\tOpcao invalida, prima enter para voltar ao menu\n");
                fflush(stdin);
                getchar();
                system("cls");
                opcao=0;
            }
        }
    }
}

void menuExame(listaExames baseExame) {
    int opcao=0;
    while(!opcao) {
        printf("1 - Adicionar Exame.\n");
        printf("2 - Alterar informações do aluno.\n");
        printf("3 - Remover Aluno.\n");
        printf("0 - Retroceder.\n");

        fflush(stdin);
        scanf("%d", &opcao);
        switch(opcao) {
            system("cls");
            case 1: {
                criaExame(baseExame);
                opcao=0;
                system("cls");
                break;
            }
            case 2: {
                opcao=0;
                system("cls");
                break;
            }
            case 3: {

                opcao=0;
                system("cls");
                break;
            }
            case 0: {
                return;
            }
            default:{
                printf("\n\tOpcao invalida, prima enter para voltar ao menu");
                fflush(stdin);
                getchar();
                system("cls");
                opcao = 0;
            }
            system("cls");
        }
    }
}

void menuMain(listaAlunos baseAluno,listaExames baseExame,listaDisciplinas baseDisciplina) {
    int opcao=0;
    while(!opcao) {
        printf("1 - Criar, alterar ou apagar dados dos alunos.\n");
        printf("2 - Criar, alterar ou apagar dados das disciplinas.\n");
        printf("3 - Gerir exames.\n");
        printf("4 - Apagar exames realizados.\n");
        printf("5 - Listar exames.\n");
        printf("6 - Inscrever alunos em exames.\n");
        printf("7 - Listar alunos inscritos do exame.\n");
        printf("8 - Listar exames do aluno.\n");
        printf("9 - Verificação de salas para o exame.\n");
        printf("0 - Sair.\n");

        fflush(stdin);
        scanf("%d", &opcao);
        system("cls");

        switch(opcao) {
            system("cls");
            case 1: {
                menuAluno(baseAluno);
                system("cls");
                opcao=0;
                break;
            }
            case 2: {
                menuDisciplina(baseDisciplina);
                system("cls");
                opcao=0;
                break;
            }
            case 3: {
                menuExame(baseExame);
                system("cls");
                opcao = 0;
                break;
            }
            case 4: {
                system("cls");
                break;
            }
            case 5: {
                system("cls");
                break;
            }
            case 6: {
                system("cls");
                break;
            }
            case 7: {
                system("cls");
                opcao=0;
                break;
            }
            case 8: {
                system("cls");
                break;
            }
            case 9: {
                system("cls");
                break;
            }
            case 0: {
                return;
            }
            default: {
                printf("\n\tOpcao invalida, prima enter para voltar ao menu\n");
                fflush(stdin);
                getchar();
                system("cls");
                opcao=0;
            }
        }
    }
}

int main()
{
    listaAlunos baseAluno = dataAlunos();
    listaExames baseExame = dataExames();
    listaDisciplinas baseDisciplina = dataDisciplinas();

    menuMain(baseAluno,baseExame,baseDisciplina);
    return 0;
}
