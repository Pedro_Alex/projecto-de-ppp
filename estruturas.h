#ifndef ESTRUTURAS_H_INCLUDED
#define ESTRUTURAS_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX 100

typedef struct info_listaExames *listaExames;
typedef struct info_listaAlunos *listaAlunos;
typedef struct info_listaDisciplinas *listaDisciplinas;

typedef struct data {
    int dia, mes, ano, hora, minutos;
}Data;

typedef struct aluno {
    int anoAluno, regime, numAluno;
    char curso[MAX];     /*??? REGIME COMO CHAR OU COMO INTEIRO*/
}Aluno;

typedef struct disciplina {
    char nomeDisciplina[MAX], nomeDocente[MAX];
}Disciplina;

typedef struct exame {
    Disciplina d;
    Data* dataExame;
    int epocaExame, sala, duracao;
    listaAlunos alunosIncritos;
}Exame;

typedef struct info_listaAlunos {   /*lista duplamente ligada*/
    Aluno student;
    listaAlunos next;
}noListaAlunos;

typedef struct info_listaExames {
    Exame e;
    listaExames next;
}noListaExames;

typedef struct info_listaDisciplinas {
    Disciplina d;
    listaDisciplinas next;
}noListaDisciplinas;

void menuAluno(listaAlunos);
void menuDisciplina(listaDisciplinas);
void menuMain(listaAlunos,listaExames,listaDisciplinas);

listaAlunos dataAlunos(void);
int listaAlunoVazia(listaAlunos);
listaAlunos destroiAluno(listaAlunos);
void eliminaAlunoLista (listaAlunos, int);
int eliminaNoAluno (listaAlunos baseAluno, int num);
listaAlunos procura_listaAlunos(listaAlunos baseAluno, int num);
void printAlunoLista(listaAlunos);

void criaAluno(listaAlunos);
void alteraAluno(listaAlunos);
void eliminaAluno(listaAlunos);

listaExames dataExames(void);
#endif // ESTRUTURAS_H_INCLUDED
