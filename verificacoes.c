
int verificaData(int dia, int mes, int ano) {
    if(mes >1 && mes < 12) {
        if (dia > 1 && dia < 31) {
            if (mes == 2 || mes == 4 || mes == 6 || mes == 9 || mes == 11) { /*Se o mes tem 30 dias ou menos*/
                if (mes == 2) {
                    if (dia > 28) return 0;
                    else return 1;
                }
                else {
                    if (dia > 30) return 0;
                    else return 1;
                }
            }
            else return 1; /*se o mes tem 31 dias*/
        }
        else return 0;
    }
    else return 0;
}

int minutosParaHoras(int horas, int minutos) {
    while (minutos > 60) {
        minutos -=60;
        horas++;
    }
}

int horasParaMinutos(int horas, int minutos) {
    while(horas!=0) {
        minutos +=60;
        horas--;
    }
    return minutos;
}

int verificaDataExame(listaExames baseExame, Data horario) {
    listaExames tmp = baseExame->next;
    int flag, time = horasParaMinutos(horario.hora, horario.minutos);
    while (tmp->next != NULL) {
        flag = 1;
        if (tmp->e.dataExame->ano == horario.ano && tmp->e.dataExame->mes == horario.mes && tmp->e.dataExame->dia == horario.dia) {
                    flag == 0;
                    break;
                }
    tmp = tmp->next;
    }
    return flag;    /*retorna 1 se nao existir nenhum exame nesse dia*/
}

int verificaSalaExame(listaExames baseExame, int sala) {
    listaExames tmp = baseExame->next;
    int flag;
    while (tmp->next != NULL) {
        flag = 1;
        if (tmp->e.sala == sala) {
            flag == 0;
            break;
        }
    tmp = tmp->next;
    }
    return flag; /*retorna 1 se nao existir ao algum exame para a sala*/
}




